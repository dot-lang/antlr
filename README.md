# ANTLR grammar and Java parser for DOT

## Description
[DOT](https://www.graphviz.org/doc/info/lang.html) is a graph description language, developed as a part of the [Graphviz](https://www.graphviz.org/) project. [ANTLR](https://www.antlr.org/) is a powerful parser generator. This project uses ANTLR to generate a Java parser for the DOT language from an [existing](https://github.com/antlr/grammars-v4/blob/master/dot/DOT.g4) DOT grammar. The provided parser is intended to build other tools.

<!--
***
## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.
-->

## Installation
<!-- Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection. -->
This is a _library_ project; it's not intended to be used as a standalone program. At the moment, it's not possible to install it. Instead, you could [Build](BUILD.md) it or [Download](README.md#download) it as a ready to use JAR file.
<!-- Debian/Ubuntu apt/flatpak/snap instructions could be here. -->

## Building
See the following to [BUILD](BUILD.md) this project from the source code.
<!-- add dependency to antlr4-runtime information -->

## Download
The latest compiled JAR version can be downloaded from [here](https://gitlab.com/zimpl-lang/bin/-/raw/main/dot-parser-0.1.jar).
<!-- TODO: when available, add maven/gradle coordinates . -->
<!-- read the [Dependencies](README.md#dependencies) Section. -->

<!--
## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.
-->
<!-- explain how to add as a dependency -->


<!--
## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.
-->

## Authors and acknowledgment
File [DOT.g4](DOT.g4) has copyright © 2013 by Terence Parr.

## License
See the [LICENCE](LICENCE) file.

<!--
## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
-->
