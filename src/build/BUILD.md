# Building instructions


## Generate parser files

### Manual creation in linux
In the root project folder, there's the `build-antlr-dot-parser.sh` shell script that reads ANTLR grammar file DOT.g4 to fill the `src-gen` folder with the java source code for the parser.

### Eclipse IDE external builder
- Select Project > Properties > Builders > New > Program.
- In the Main tab, complete Location with the shell script file used for manual generation.
- In the Build Options tab, select "Specify working set...", click on "Specify Resources..." and tick the \*.g4 files.


## Generate jar file `dot-parser.jar`

This instructions assume you're using the Eclipse IDE. If you adapt them for other IDEs, please contribute that back.

### Automatic generation with the Eclipse IDE
 - Open `build.jardesc` and click "Finish".

### Manual generation with the Eclipse IDE
- Select the project in the Package Explorer.
- In the menu bar, choose File > Export > Java > JAR file.
- Deselect source resources.
- Select the `src-gen` and `src/main/java` folders.
- Fill in "JAR file" with "dot-parser-${version}.jar".
- Click "Finish".

**TODO**: autocomplete ${version}

## Create dot-parser-sources.zip
**TODO**

## Create dot-parser-${version}-javadoc.zip
**TODO**

## Publish grammar files
**TODO**
