package dot;

import java.io.IOException;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.TokenStream;

import dot.antlr.DOTLexer;
import dot.antlr.DOTParser;

public class DotParser extends DOTParser {
	
	public static DotParser fromFilename(String filename)
	throws IOException {
		CharStream input = CharStreams.fromFileName(filename);
		return fromCharStream(input);
	}
	
	public static DotParser fromString(String text) {
		CharStream input = CharStreams.fromString(text);
		return fromCharStream(input);
	}
	
	static DotParser fromCharStream(CharStream input) {
		// create a lexer that feeds off of input CharStream
		DOTLexer lexer = new DOTLexer(input);
		// create a buffer of tokens pulled from the lexer
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		// create a parser that feeds off the tokens buffer
		DotParser result = new DotParser(tokens);
//		setupErrorListenersFor(List.of(lexer, result), attachDiagnostic);
		return result;
	}
	
	protected DotParser(TokenStream input) { super(input); }
	
}
